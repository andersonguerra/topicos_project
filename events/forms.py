from django import forms
from .models import Event, Programacao

class EventForm1(forms.ModelForm):
    class Meta:
        model = Event
        fields = ['nome', 'descricao', 'email_contato', 'data_inicio', 'data_fim',
            'dadosLocal']


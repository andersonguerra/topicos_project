from django.db import models
from jsonfield import JSONField
from account.models import User


class Event(models.Model):
    nome = models.CharField('Nome', max_length=50)
    descricao = models.TextField('Descrição')
    email_contato = models.EmailField()
    data_inicio = models.DateField()
    data_fim = models.DateField()
    #isPresencial = models.BooleanField()
    dadosLocal = JSONField() #cep, endereco, bairro, cidade, estado ou URL caso online
    #campos_adicionais = JSONField()

    def __str__(self):
        self.nome


class Programacao(models.Model):
    nome = models.CharField(max_length=20)
    evento = models.ForeignKey(Event)

    def __str__(self):
        self.nome


class PermissoesEvento(models.Model):
    user = models.ForeignKey(User)
    event = models.ForeignKey(Event)
    permission = models.PositiveIntegerField() # 1 organizador, 2 credenciador, 3 participante?
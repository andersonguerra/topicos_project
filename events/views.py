from django.shortcuts import render, redirect
from django.core.urlresolvers import reverse_lazy

from django.views.generic import (CreateView, ListView, UpdateView, FormView)
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required

from formtools.wizard.views import SessionWizardView

from .models import Event, PermissoesEvento
from .forms import EventForm1


class CreateEvent(LoginRequiredMixin, CreateView):
    model = Event
    form_class = EventForm1
    template_name = 'events/event_register.html'
    success_url = reverse_lazy('account:index_user')
    


createEvent = CreateEvent.as_view()

@login_required
def create_event(request):
    eventForm1 = EventForm1()
    if request.method == 'POST':
        form = EventForm1(request.POST)
        if form.is_valid():
            auxEvent = form.save()
            permissoes = PermissoesEvento()
            permissoes.user = request.user
            permissoes.event = auxEvent
            permissoes.permission = 1
            permissoes.save()
            return redirect(reverse_lazy('account:index_user'))
    else:
        form = EventForm1()
    return render(request, 'events/event_register.html', {'form': form})



# os códigos abaixo foram testes para fazer um criador de vento semelhante a um wizard
# uma pagina para informações, outra para inscrições e por fim o site
# aí então o evento era salvo no banco, mas parei de fazer isso no momento e agora é
# usada uma página apenas
"""TEMPLATES = {
    'informacoes': 'events/event_register_informacoes.html',
    'inscricoes': 'events/event_register_inscricoes.html'
}

class CreateEvent(SessionWizardView):
    instance = Event()
    
    def get_template_names(self):
        return [TEMPLATES[self.steps.current]]

    def dispatch(self, request, *args, **kwargs):
        return super(CreateEvent, self).dispatch(request, *args, **kwargs)

    def get_form_instance(self, step):
        return self.instance

    def done(self, form_list, **kwargs):
        self.save_model()
        return reverse_lazy('account:index_user')"""
from django.conf.urls import url

from . import views, forms

"""FORMS = [
    ('informacoes', forms.EventForm1),
    ('inscricoes', forms.EventForm2),
]"""

urlpatterns = [
#    url(r'^criar/', views.CreateEvent.as_view(FORMS), name='create_event'),
    url(r'^criar/', views.create_event, name='create_event'),
]
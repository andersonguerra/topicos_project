function verificarSenha(senha1, senha2, er1, er2){
    if (senha1.value.length >= 8){
        er1.innerHTML = "";
        if (senha1.value === senha2.value){
            er2.innerHTML = "";
        }
        else{
            er2.innerHTML = "As senhas devem ser iguais";
        }
    }
    else{
        er1.innerHTML = "A senha deve ter pelo menos 8 caracteres";
    }
}

function verificarCPF(cpf, erCpf){
    if( testarCPF(cpf)){
        erCpf.innerHTML="";
    }
    else{
        erCpf.innerHTML="CPF inválido";
    }
}


function testarCPF(strCPF) {
    strCPF= strCPF.value;
    var Soma;
    var Resto;
    Soma = 0;
	  if (strCPF == "00000000000") return false;

	  for (i=1; i<=9; i++) Soma = Soma + parseInt(strCPF.substring(i-1, i)) * (11 - i);
	  Resto = (Soma * 10) % 11;

    if ((Resto == 10) || (Resto == 11))  Resto = 0;
    if (Resto != parseInt(strCPF.substring(9, 10)) ) return false;

	  Soma = 0;
    for (i = 1; i <= 10; i++) Soma = Soma + parseInt(strCPF.substring(i-1, i)) * (12 - i);
    Resto = (Soma * 10) % 11;

    if ((Resto == 10) || (Resto == 11))  Resto = 0;
    if (Resto != parseInt(strCPF.substring(10, 11) ) ) return false;
    return true;
}


function testarEmail(email){
    email = email.value;
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function verificarEmail(email,erEmail){
    if(testarEmail(email)){
        erEmail.innerHTML="";
    }
    else{
        erEmail.innerHTML="Endereço inválido";
    }
} 

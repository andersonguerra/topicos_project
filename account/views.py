from django.shortcuts import render
from django.core.urlresolvers import reverse_lazy

from django.views.generic import (CreateView, ListView, UpdateView, FormView)
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required

from events.models import Event, PermissoesEvento
from .models import User
from .forms import UserForm


class CreateUser(CreateView):
    model = User
    form_class = UserForm
    #fields = ['first_name', 'last_name', 'email', 'password']
    template_name = 'account/user_register.html'
    success_url = reverse_lazy('account:index_user')


class UpdateUser(LoginRequiredMixin, UpdateView):
    model = User
    template_name = 'account/user_update.html'
    fields = ['first_name', 'last_name', 'email']
    success_url = reverse_lazy('account:index_user')

    def get_object(self):
        return self.request.user


class UpdatePasswordView(LoginRequiredMixin, FormView):
    template_name = 'account/user_updatePassword.html'
    success_url = reverse_lazy('account:index_user')
    form_class = PasswordChangeForm

    def get_form_kwargs(self):
        kwargs = super(UpdatePasswordView, self).get_form_kwargs()
        kwargs['user'] = self.request.user
        return kwargs

    def form_valid(self, form):
        form.save()
        return super(UpdatePasswordView, self).form_valid(form)


class ListUser(LoginRequiredMixin,ListView):
    model = User
    template_name = 'account/user_dashboard.html'

@login_required
def indexUser(request):
    permissoesUser = PermissoesEvento.objects.filter(user=request.user, permission=1)
    return render(request, 'account/user_dashboard.html', {'permissoes': permissoesUser})

createUser = CreateUser.as_view()
updateUser = UpdateUser.as_view()
updateUserPassword = UpdatePasswordView.as_view()
#listUser = ListUser.as_view()

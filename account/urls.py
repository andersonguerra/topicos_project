from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.indexUser, name='index_user'),
    url(r'^alterar-dados/$', views.updateUser, name='update_user'),
    url(r'^alterar-senha/$', views.updateUserPassword, name='update_password'),
    url(r'^criar/', views.createUser, name='create_user'),
]